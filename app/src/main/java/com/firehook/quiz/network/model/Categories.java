package com.firehook.quiz.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Categories {
    @SerializedName("uid")
    @Expose
    public Integer uid;
    @SerializedName("secondaryCid")
    @Expose
    public String secondaryCid;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("type")
    @Expose
    public String type;
}
