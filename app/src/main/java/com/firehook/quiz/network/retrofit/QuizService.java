package com.firehook.quiz.network.retrofit;

import com.firehook.quiz.network.model.QuizItems;

import io.reactivex.Observable;
import retrofit2.http.GET;

public interface QuizService {
    @GET("v1/quizzes/0/100")
    Observable<QuizItems> getQuizItems();
}
