package com.firehook.quiz;

import android.app.Application;

import com.firehook.quiz.injection.component.ApplicationComponent;
import com.firehook.quiz.injection.component.DaggerApplicationComponent;
import com.firehook.quiz.injection.module.ApplicationModule;
import com.firehook.quiz.injection.module.NetworkModule;
import com.firehook.quiz.injection.module.PreferenceModule;

import java.util.Locale;

import timber.log.Timber;

public class QuizApp extends Application {

    public static ApplicationComponent sAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        sAppComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .networkModule(new NetworkModule())
                .preferenceModule(new PreferenceModule())
                .build();

        if (BuildConfig.DEBUG){
            Timber.plant(new Timber.DebugTree(){
                @Override
                protected String createStackElementTag(StackTraceElement element) {
                    return String.format(Locale.getDefault(), "[TIMBER] %s.%s() [#%d]",
                            super.createStackElementTag(element),
                            element.getMethodName(),
                            element.getLineNumber());
                }
            });
        }
    }
}
