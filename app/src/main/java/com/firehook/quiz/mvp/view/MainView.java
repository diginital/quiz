package com.firehook.quiz.mvp.view;

import com.arellomobile.mvp.MvpView;

public interface MainView extends MvpView {
    void showToastMessage(String msg);
}
