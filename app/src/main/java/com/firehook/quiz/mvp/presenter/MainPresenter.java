package com.firehook.quiz.mvp.presenter;

import android.content.SharedPreferences;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.firehook.quiz.QuizApp;
import com.firehook.quiz.mvp.view.MainView;

import javax.inject.Inject;

import okhttp3.OkHttpClient;

@InjectViewState
public class MainPresenter extends MvpPresenter<MainView> {
    @Inject OkHttpClient mOkHttpClient;
    @Inject SharedPreferences mPrefs;

    public MainPresenter() {
        QuizApp.sAppComponent.inject(this);
    }

    void showMessage(){
        if (mPrefs == null){
            getViewState().showToastMessage("NULL!!!");
        }else {
            getViewState().showToastMessage("Not NULL!!!");
        }
    }
}
