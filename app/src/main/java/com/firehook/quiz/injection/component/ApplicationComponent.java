package com.firehook.quiz.injection.component;

import com.firehook.quiz.injection.module.ApplicationModule;
import com.firehook.quiz.injection.module.NetworkModule;
import com.firehook.quiz.injection.module.PreferenceModule;
import com.firehook.quiz.mvp.presenter.MainPresenter;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component (modules = {ApplicationModule.class, NetworkModule.class, PreferenceModule.class})
public interface ApplicationComponent {
    void inject(MainPresenter presenter);
}
