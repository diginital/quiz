package com.firehook.quiz.injection.module;

import android.content.Context;

import com.firehook.quiz.QuizApp;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {
    private QuizApp application;

    public ApplicationModule(QuizApp application) {
        this.application = application;
    }

    @Provides @Singleton
    Context provideContext(){
        return this.application.getApplicationContext();
    }
}
