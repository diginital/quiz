package com.firehook.quiz.injection.module;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class PreferenceModule {

    @Provides @Singleton
    SharedPreferences providePreferences(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

}
